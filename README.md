# rotationVerify

#### 介绍
基于php后端的旋转图片验证码

#### 软件架构
php + jquery

#### 安装教程

本项目是基于thinkphp6的演示项目，搭建好运行即可。

#### 一睹芳容
![](./public/screenshoot/1.png)
![](./public/screenshoot/2.png)
![](./public/screenshoot/3.png)  

#### 使用说明

1、在使用的页面 引入 `jquery.min.js` 和 `captcha.js`，并放置如下的代码
```angular2html
var myRotateVerify = new RotateVerify('#rotateWrap1', {
    initText: '滑动将图片转正',//默认
    slideImage: ['/static/image/1.png', '/static/image/2.png', '/static/image/4.png', '/static/image/5.png'], //arr  [imgsrc1,imgsrc2] 或者str 'imgsrc1'
    initUrl: "/index/initData",
    verifyUrl: "/index/verify",
    verifyParam: {},
    getSuccessState: function (res) {
        // 验证通过 返回  true;
        console.log('验证成功', res);
    }
})
```
这里的  
slideImage： 就是随机变换的背景  
initUrl：  是初始化基础参数的接口，填写您的实际接口即可  
verifyUrl：  是旋转最后验证的接口地址  
verifyParam：校验的额外参数，如果您需要在 `verify` 接口中做额外的操作，比如说发短信之类的行为，就可以在校验旋转验证码成功后，直接通过 `verifyParam` 给的参数直接做了

2、样式的问题  
本项目自带样式，如果您要修改样式，则需要注意
```angular2html
<div class="box" style="display:block;">
    <img src="/static/cw.png" class="cuo">
    <span class="top-s">身份验证</span>
    <span class="top-x">拖动滑块，使图片角度为正</span>
    <div id="rotateWrap1">
        <div class="rotateverify-contaniner">
            <div class="rotate-can-wrap">
                <canvas class="rotateCan rotate-can" width="200" height="200"></canvas>
                <div class="statusBg status-bg"></div>
            </div>
            <div class="control-wrap slideDragWrap">
                <div class="control-tips">
                    <p class="c-tips-txt cTipsTxt">滑动将图片转正</p>
                </div>
                <div class="control-bor-wrap controlBorWrap"></div>
                <div class="control-btn slideDragBtn">
                    <i class="control-btn-ico"></i>
                </div>
            </div>
        </div>
    </div>
</div>
```

`slideDragWrap` 和 `slideDragBtn` 这两个样式名需要保留，不可轻易更改，因为这两个样式名需要参与js中的操作，如果这里修改了，
`captcha.js` 中的对应部分应一起修改

3、后端验证的原理  
1、通过 `initData` 接口生成必要的参数
```php
$dragWrap = input('post.dragWrap');
$dragBtn = input('post.dragBtn');

$slideAreaNum = 10; // 误差值，这个值可以改
$randRot = rand(30, 270);

$sucLenMin = (360 - $slideAreaNum - $randRot) * ($dragWrap - $dragBtn) / 360;
$sucLenMax = (360 + $slideAreaNum - $randRot) * ($dragWrap - $dragBtn) / 360;

session('captcha', [
    'min' => $sucLenMin,
    'max' => $sucLenMax
]);
```
随机一个旋转的 角度 `randRot`, 并且是定一个 `slideAreaNum` 误差值，因为没人能完全把图片旋转到最正的位置。
本案例通过的 session 存储这个误差值的范围。$sucLenMin 和 $sucLenMax。当然我是基于tp6的，tp6的session可以共享到redis中
如果您不是 tp6，可以通过别的手段，让前端拿到这个存储的标识，方便后面的校验。

2、校验接口 `verify`
```php
$disLf = input('post.disLf');

$verifyData = session('captcha');
if ($verifyData['min'] <= $disLf && $verifyData['max'] >= $disLf) {
    return json(['code' => 0, 'data' => [], 'msg' => '验证成功']);
}

return json(['code' => -1, 'data' => [], 'msg' => '验证失败']);
```
拿到前端滑动的值，校验即可。没什么好说的了。

3、校验完成后，就可以在js 的 `getSuccessState` 做一些别的逻辑了。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特别鸣谢

本项目前端是基于 https://gitee.com/xiaomadagege/image-rotation-verification 修改而来。